﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Serializable;
using AIStudio.Wpf.DiagramDesigner.Serializable.ViewModels;
using AIStudio.Wpf.Flowchart.ViewModels;

namespace AIStudio.Wpf.Flowchart.Models
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="AIStudio.Util.DiagramEntity.DiagramNode" />
    public class FlowchartNode : DiagramNode
    {
        /// <summary>
        /// Gets or sets the kind.
        /// </summary>
        /// <value>
        /// The kind.
        /// </value>
        public NodeKinds Kind
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the user ids.
        /// </summary>
        /// <value>
        /// The user ids.
        /// </value>
        public IEnumerable<string> UserIds
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the role ids.
        /// </summary>
        /// <value>
        /// The role ids.
        /// </value>
        public IEnumerable<string> RoleIds
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the type of the act.
        /// </summary>
        /// <value>
        /// The type of the act.
        /// </value>
        public string ActType
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public string StatusColor
        {
            get; set;
        }

        public override DiagramItemViewModel ToNodel(IDiagramViewModel diagramViewModel)
        {
            FlowNode flowNode = null;
            switch (Kind)
            {
                case NodeKinds.Start:
                    {
                        flowNode = new StartFlowNode(diagramViewModel);
                        break;
                    }

                case NodeKinds.End:
                    {
                        flowNode = new EndFlowNode(diagramViewModel);
                        break;
                    }
                case NodeKinds.Decide:
                    {
                        flowNode = new DecideFlowNode(diagramViewModel);
                        break;
                    }
                case NodeKinds.COBegin:
                    {
                        flowNode = new COBeginFlowNode(diagramViewModel);
                        break;
                    }
                case NodeKinds.COEnd:
                    {
                        flowNode = new COEndFlowNode(diagramViewModel);
                        break;
                    }
                case NodeKinds.Middle:
                    {
                        var flowchartNodelModel = new MiddleFlowNode(diagramViewModel);
                        flowNode = flowchartNodelModel;
                        flowchartNodelModel.UserIds = UserIds?.ToList();
                        flowchartNodelModel.RoleIds = RoleIds?.ToList();
                        flowchartNodelModel.ActType = ActType;
                        break;
                    }
                default:
                    {
                        var flowNodelModel = new FlowNode(diagramViewModel, NodeKinds.Normal);
                        flowNode = flowNodelModel;
                        break;
                    }

            }

            flowNode.StatusColor = StatusColor;
            flowNode.Kind = Kind;

            return flowNode;
        }


    }
}
