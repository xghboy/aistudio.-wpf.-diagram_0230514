﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Helpers;
using AIStudio.Wpf.Flowchart;
using AIStudio.Wpf.Flowchart.Models;
using AIStudio.Wpf.Flowchart.ViewModels;

namespace AIStudio.Wpf.Flowchart.ViewModels
{
    public class FlowchartToolBoxViewModel
    {
        private List<ToolBoxData> toolBoxItems = new List<ToolBoxData>();

        public FlowchartToolBoxViewModel()
        {
            var screenScale = ScreenHelper.ResetScreenScale();
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.Start, typeof(StartFlowNode), 80, 60, new Size(100 / screenScale, 80/ screenScale)));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.End, typeof(EndFlowNode), 80, 60, new Size(100 / screenScale, 80 / screenScale)));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.Middle, typeof(MiddleFlowNode), 80, 60, new Size(100 / screenScale, 80 / screenScale)));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.Decide, typeof(DecideFlowNode), 80, 60, new Size(100 / screenScale, 80 / screenScale)));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.COBegin, typeof(COBeginFlowNode), 80, 60, new Size(100 / screenScale, 80 / screenScale)));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.COEnd, typeof(COEndFlowNode), 80, 60, new Size(100 / screenScale, 80 / screenScale)));
        }

        public List<ToolBoxData> ToolBoxItems
        {
            get
            {
                return toolBoxItems;
            }
        }
    }  
   
}
