﻿using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.DiagramDesigner.Additionals.Extensions.Models
{
    public class PathDesignerItem : DesignerItemBase
    {
        public PathDesignerItem()
        {

        }

        public PathDesignerItem(DesignerItemViewModelBase item) : base(item) { }
    }
}
