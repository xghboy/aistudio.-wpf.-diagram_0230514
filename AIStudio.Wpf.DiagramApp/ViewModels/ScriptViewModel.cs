﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Media;
using AIStudio.Wpf.DiagramApp.Models;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.Script.Core.Helpers;
using AIStudio.Wpf.Script.Core.Models;
using AIStudio.Wpf.Script.Core.ViewModels;
using AIStudio.Wpf.Script.ViewModels;
using AIStudio.Wpf.SFC;
using Microsoft.CodeAnalysis;

namespace AIStudio.Wpf.DiagramApp.ViewModels
{
    public class ScriptViewModel : PageViewModel
    {
        static ScriptViewModel()
        {
            TextEditorThemeHelper.SetCurrentTheme("VS2019_Dark");
        }

        public ScriptViewModel(string title, string status, DiagramType diagramType) : base(title, status, diagramType)
        {
            Init(true);
        }

        public ScriptViewModel(string filename, DiagramDocument diagramDocument) : base(filename, diagramDocument)
        {
            if (DiagramViewModel != null)
            {

            }

            readDataTimer.Elapsed += timeCycle;
            readDataTimer.Interval = 1000;
            readDataTimer.AutoReset = false;
            readDataTimer.Start();
        }

        protected override void InitDiagramViewModel()
        {
            base.InitDiagramViewModel();

            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;
        }

        private System.Timers.Timer readDataTimer = new System.Timers.Timer();
        protected override void Init(bool initNew)
        {
            base.Init(initNew);

            ConsoleLogViewModel logNode = new ConsoleLogViewModel(DiagramViewModel) { Name = "Log", Left = 150, Top = 420 };
            DiagramViewModel.Add(logNode);

            IntScriptViewModel intLog = new IntScriptViewModel(DiagramViewModel) { Name = "Int", Left = 40, Top = 220 };
            DiagramViewModel.Add(intLog);

            ColorBoxFactoryScriptViewModel factoryNode = new ColorBoxFactoryScriptViewModel(DiagramViewModel) { Name = "ColorBoxFactory", Left = 150, Top = 150 };
            DiagramViewModel.Add(factoryNode);

            BoolScriptViewModel boolNode = new BoolScriptViewModel(DiagramViewModel) { Name = "Bool", TrueString = "形状", FalseString = "颜色", Left = 410, Top = 190 };
            DiagramViewModel.Add(boolNode);

            ColorBoxSwitchScriptViewModel switchNode = new ColorBoxSwitchScriptViewModel(DiagramViewModel) { Name = "ColorBoxSwitch", Left = 500, Top = 150 };
            DiagramViewModel.Add(switchNode);

            TextScriptViewModel textNode = new TextScriptViewModel(DiagramViewModel) { Name = "Text", Text = "AIStudio Diagram", Left = 150, Top = 50, ItemWidth = 450, ItemHeight = 60 };
            DiagramViewModel.Add(textNode);

            ColorBoxIfScriptViewModel ifNode = new ColorBoxIfScriptViewModel(DiagramViewModel) { Name = "ColorBoxIf", Left = 660, Top = 60 };
            DiagramViewModel.Add(ifNode);

            ColorBoxCollectorScriptViewModel collectorNode1_1 = new ColorBoxCollectorScriptViewModel(DiagramViewModel) { Name = "Collector1_1", Left = 790, Top = 40 };
            collectorNode1_1.Tag = new ColorBoxModel("红色|圆形", "M 10,20 A 20,20 0 1 1 50,20 A 20,20 0 1 1 10,20", Colors.Red);
            DiagramViewModel.Add(collectorNode1_1);

            ColorBoxCollectorScriptViewModel collectorNode1_2 = new ColorBoxCollectorScriptViewModel(DiagramViewModel) { Name = "Collector1_2", Left = 790, Top = 120 };
            collectorNode1_2.Tag = new ColorBoxModel("红色|圆形", "M 10,20 A 20,20 0 1 1 50,20 A 20,20 0 1 1 10,20", Colors.Red);
            DiagramViewModel.Add(collectorNode1_2);

            ColorBoxCollectorScriptViewModel collectorNode2 = new ColorBoxCollectorScriptViewModel(DiagramViewModel) { Name = "Collector2", Left = 790, Top = 220 };
            collectorNode2.Tag = new ColorBoxModel("橙色|三角形", "M1,21H23L12,2", Colors.Orange);
            DiagramViewModel.Add(collectorNode2);

            ColorBoxCollectorScriptViewModel collectorNode3 = new ColorBoxCollectorScriptViewModel(DiagramViewModel) { Name = "Collector3", Left = 790, Top = 305 };
            collectorNode3.Tag = new ColorBoxModel("黄色|方形", "M3,3V21H21V3", Colors.Yellow);
            DiagramViewModel.Add(collectorNode3);

            ColorBoxCollectorScriptViewModel collectorNode4 = new ColorBoxCollectorScriptViewModel(DiagramViewModel) { Name = "Collector4", Left = 790, Top = 390 };
            collectorNode4.Tag = new ColorBoxModel("绿色|菱形", "M 0,20 L 30 0 L 60,20 L 30,40 Z", Colors.Green);
            DiagramViewModel.Add(collectorNode4);

            ColorBoxCollectorScriptViewModel collectorNode5 = new ColorBoxCollectorScriptViewModel(DiagramViewModel) { Name = "Collector5", Left = 790, Top = 475 };
            collectorNode5.Tag = new ColorBoxModel("青色|梯形", "M 0 0 H 60 L 50 40 H 10 Z", Colors.Cyan);
            DiagramViewModel.Add(collectorNode5);

            ColorBoxCollectorScriptViewModel collectorNode6 = new ColorBoxCollectorScriptViewModel(DiagramViewModel) { Name = "Collector6", Left = 790, Top = 560 };
            collectorNode6.Tag = new ColorBoxModel("蓝色|五角星", "M 9,2 11,7 17,7 12,10 14,15 9,12 4,15 6,10 1,7 7,7 Z", Colors.Blue);
            DiagramViewModel.Add(collectorNode6);

            ColorBoxCollectorScriptViewModel collectorNode7 = new ColorBoxCollectorScriptViewModel(DiagramViewModel) { Name = "Collector7", Left = 790, Top = 645 };
            collectorNode7.Tag = new ColorBoxModel("紫色|六边形", "M 0,20 L 10,0  H 50 L 60,20 L 50,40 H10 Z", Colors.Purple);
            DiagramViewModel.Add(collectorNode7);

            ConnectionViewModel int_factoryConnector = new ConnectionViewModel(DiagramViewModel, intLog.Output[0], factoryNode.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(int_factoryConnector);

            ConnectionViewModel bool_switchConnector = new ConnectionViewModel(DiagramViewModel, boolNode.Output[0], switchNode.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(bool_switchConnector);

            ConnectionViewModel factory_switchConnector = new ConnectionViewModel(DiagramViewModel, factoryNode.Output[0], switchNode.Input[1], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            factory_switchConnector.AnimationViewModel.Repeat = false;
            factory_switchConnector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(factory_switchConnector);

            ConnectionViewModel switch_textConnector = new ConnectionViewModel(DiagramViewModel, switchNode.Output[0], textNode.Input[0], DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            switch_textConnector.SetPathGeneratorParameter(null, null, 5, 10);
            DiagramViewModel.Add(switch_textConnector);

            ConnectionViewModel switch_ifConnector = new ConnectionViewModel(DiagramViewModel, switchNode.Output[1], ifNode.Input[2], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            switch_ifConnector.AnimationViewModel.Repeat = false;
            switch_ifConnector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(switch_ifConnector);

            ConnectionViewModel if_collector1_1Connector = new ConnectionViewModel(DiagramViewModel, ifNode.Output[0], collectorNode1_1.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            if_collector1_1Connector.AnimationViewModel.Repeat = false;
            if_collector1_1Connector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(if_collector1_1Connector);

            ConnectionViewModel if_collector1_2Connector = new ConnectionViewModel(DiagramViewModel, ifNode.Output[1], collectorNode1_2.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            if_collector1_2Connector.AnimationViewModel.Repeat = false;
            if_collector1_2Connector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(if_collector1_2Connector);

            ConnectionViewModel collector1_1_if_Connector = new ConnectionViewModel(DiagramViewModel, collectorNode1_1.Output[0], ifNode.Input[0], DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            collector1_1_if_Connector.SetPathGeneratorParameter(null, null, 5, 30);
            DiagramViewModel.Add(collector1_1_if_Connector);

            ConnectionViewModel collector1_2_if_Connector = new ConnectionViewModel(DiagramViewModel, collectorNode1_2.Output[0], ifNode.Input[1], DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            collector1_2_if_Connector.SetPathGeneratorParameter(null, null, 5, 10);
            DiagramViewModel.Add(collector1_2_if_Connector);

            ConnectionViewModel switch_collector2Connector = new ConnectionViewModel(DiagramViewModel, switchNode.Output[2], collectorNode2.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            switch_collector2Connector.AnimationViewModel.Repeat = false;
            switch_collector2Connector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(switch_collector2Connector);

            ConnectionViewModel switch_collector3Connector = new ConnectionViewModel(DiagramViewModel, switchNode.Output[3], collectorNode3.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            switch_collector3Connector.AnimationViewModel.Repeat = false;
            switch_collector3Connector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(switch_collector3Connector);

            ConnectionViewModel switch_collector4Connector = new ConnectionViewModel(DiagramViewModel, switchNode.Output[4], collectorNode4.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            switch_collector4Connector.AnimationViewModel.Repeat = false;
            switch_collector4Connector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(switch_collector4Connector);

            ConnectionViewModel switch_collector5Connector = new ConnectionViewModel(DiagramViewModel, switchNode.Output[5], collectorNode5.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            switch_collector5Connector.AnimationViewModel.Repeat = false;
            switch_collector5Connector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(switch_collector5Connector);

            ConnectionViewModel switch_collector6Connector = new ConnectionViewModel(DiagramViewModel, switchNode.Output[6], collectorNode6.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            switch_collector6Connector.AnimationViewModel.Repeat = false;
            switch_collector6Connector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(switch_collector6Connector);

            ConnectionViewModel switch_collector7Connector = new ConnectionViewModel(DiagramViewModel, switchNode.Output[7], collectorNode7.Input[0], DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            switch_collector7Connector.AnimationViewModel.Repeat = false;
            switch_collector7Connector.AnimationViewModel.Animation = LineAnimation.PathAnimation;
            DiagramViewModel.Add(switch_collector7Connector);

            readDataTimer.Elapsed += timeCycle;
            readDataTimer.Interval = 1000;
            readDataTimer.AutoReset = false;
            readDataTimer.Start();
        }

        private void timeCycle(object sender, ElapsedEventArgs e)
        {
            foreach (var item in DiagramViewModel.Items.OfType<LogicalGateItemViewModelBase>().OrderBy(p => p.OrderNumber).ToList())
            {
                item.Execute();
            }
            readDataTimer.Start();
        }

        public override void Dispose()
        {
            base.Dispose();

            readDataTimer.Stop();
            readDataTimer.Dispose();

            foreach (var item in DiagramViewModel.Items.OfType<LogicalGateItemViewModelBase>().OrderBy(p => p.OrderNumber).ToList())
            {
                item.Dispose();
            }
        }
    }
}
