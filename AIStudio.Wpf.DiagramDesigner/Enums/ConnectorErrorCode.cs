﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIStudio.Wpf.DiagramDesigner.Enums
{
    public enum ConnectorErrorCode
    {
        None,
        ConnErr,
        ValueErr
    }
}
