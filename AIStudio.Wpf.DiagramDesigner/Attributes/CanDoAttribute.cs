﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    [AttributeUsage(AttributeTargets.Property)]
    public class CanDoAttribute : Attribute
    {
    }
}
