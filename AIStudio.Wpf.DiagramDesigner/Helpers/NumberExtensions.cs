﻿using System.Globalization;

namespace AIStudio.Wpf.DiagramDesigner
{
    public static class NumberExtensions
    {
        public static string ToInvariantString(this double n) => n.ToString(CultureInfo.InvariantCulture);
    }
}
