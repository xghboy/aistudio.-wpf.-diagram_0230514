﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IPathGenerator
    {
        PathGeneratorResult Get(IDiagramViewModel _, ConnectionViewModel link, PointBase[] route, PointBase source, PointBase target);
    }
}
