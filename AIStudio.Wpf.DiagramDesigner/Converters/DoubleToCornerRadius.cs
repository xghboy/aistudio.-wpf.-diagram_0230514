﻿using System.Globalization;
using System.Windows;
using System.Windows.Data;


namespace AIStudio.Wpf.DiagramDesigner
{
    public class DoubleToCornerRadius : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (parameter != null)
                {
                    switch (parameter.ToString())
                    {
                        case "TopLeft":
                            return new CornerRadius(System.Convert.ToDouble(value), 0, 0, 0);

                        case "TopRight":
                            return new CornerRadius(0, System.Convert.ToDouble(value), 0, 0);

                        case "BottomRight":
                            return new CornerRadius(0, 0, System.Convert.ToDouble(value), 0);

                        case "ButtomLeft":
                            return new CornerRadius(0, 0, 0, System.Convert.ToDouble(value));
                       
                        default:
                            return new CornerRadius(System.Convert.ToDouble(value));
                    }
                }
                return new CornerRadius(System.Convert.ToDouble(value));
            }
            return new CornerRadius(0);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (parameter != null)
                {
                    switch (parameter.ToString())
                    {
                        case "TopLeft":
                            return ((CornerRadius)value).TopLeft;

                        case "TopRight":
                            return ((CornerRadius)value).TopRight;

                        case "BottomRight":
                            return ((CornerRadius)value).BottomRight;

                        case "BottomLeft":
                            return ((CornerRadius)value).BottomLeft;

                        default:
                            return ((CornerRadius)value).TopLeft;
                    }
                }
                return ((CornerRadius)value).TopLeft;
            }
            return 0.0;
        }
    }
}
