﻿using AIStudio.Wpf.DiagramDesigner;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ConectorValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[0] is LogicalConnectorInfo logicalConnectorInfo)
            {
                if (logicalConnectorInfo.ErrorCode != Enums.ConnectorErrorCode.None)
                {
                    if (parameter?.ToString() == "ToolTip")
                    {
                        return logicalConnectorInfo.ErrorMessage;
                    }
                    else
                    {
                        return logicalConnectorInfo.ErrorCode.ToString();
                    }
                }
                else if (logicalConnectorInfo.ConnectorValueType == ConnectorValueType.Bool)
                {
                    return (logicalConnectorInfo.ConnectorValue == 0) ? "F" : "T";
                }
                else if (logicalConnectorInfo.ConnectorValueType == ConnectorValueType.Int)
                {
                    return logicalConnectorInfo.ConnectorValue.ToString("0");
                }
                else if (logicalConnectorInfo.ConnectorValueType == ConnectorValueType.Real)
                {
                    return logicalConnectorInfo.ConnectorValue.ToString("f3");
                }
                else if (logicalConnectorInfo.ConnectorValueType == ConnectorValueType.JsonString)
                {
                    if (logicalConnectorInfo.ConnectorString != null)
                    {
                        JObject obj = JObject.Parse(logicalConnectorInfo.ConnectorString);
                        if (obj.ContainsKey("Text"))
                        {
                            return obj["Text"].ToString();
                        }
                    }
                }
                else
                {
                    if (parameter?.ToString() == "ToolTip")
                    {
                        return logicalConnectorInfo.ConnectorString;
                    }
                    else
                    {
                        return "...";
                    }
                }
            }
            return null;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
