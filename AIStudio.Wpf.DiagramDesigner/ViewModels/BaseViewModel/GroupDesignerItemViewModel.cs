﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// DefaultGroup
    /// </summary>
    public class GroupDesignerItemViewModel : DesignerItemViewModelBase
    {
        public GroupDesignerItemViewModel() : this(null)
        {
            
        }

        public GroupDesignerItemViewModel(IDiagramViewModel root) : base(root)
        {
            
        }

        public GroupDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public GroupDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        protected override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);

            this.IsGroup = true;
            this.IsHitTestVisible = true;
        }

        protected override void InitNew()
        {
            this.ClearConnectors();
        }

        protected override void ExecuteEditCommand(object param)
        {
        }

        public void Resize()
        {
            if (this.Root == null)  return; 

            var items = this.Root.Items.Where(p => p.ParentId == Id).OfType<DesignerItemViewModelBase>();
            RectangleBase rect = DiagramViewModelHelper.GetBoundingRectangle(items);

            this.ItemWidth = rect.Width;
            this.ItemHeight = rect.Height;
            this.Left = rect.Left;
            this.Top = rect.Top;
        }

        public void AddItem(DesignerItemViewModelBase item)
        {
            if (this.Root == null) return;

            item.ParentId = Id;
            if (item.ZIndex > this.ZIndex)
            {
                var zindex = item.ZIndex;
                item.ZIndex = this.ZIndex;
                this.ZIndex = zindex;
            }
            Resize();   
        }
    }
}
