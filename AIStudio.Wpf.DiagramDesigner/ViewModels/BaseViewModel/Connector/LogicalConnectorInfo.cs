﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Enums;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class LogicalConnectorInfo : FullyCreatedConnectorInfo
    {
        public LogicalConnectorInfo(DesignerItemViewModelBase dataItem, ConnectorOrientation orientation, bool isInnerPoint = false, bool isPortless = false, ConnectorValueType connectorValueType = ConnectorValueType.Real) : base(dataItem, orientation, isInnerPoint, isPortless)
        {
            this.ConnectorValueType = connectorValueType;
        }

        public LogicalConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, ConnectorOrientation orientation, bool isInnerPoint = false, bool isPortless = false, ConnectorValueType connectorValueType = ConnectorValueType.Real) : base(root, dataItem, orientation, isInnerPoint, isPortless)
        {
            this.ConnectorValueType = connectorValueType;
        }

        public LogicalConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, SelectableItemBase designer) : base(root, dataItem, designer)
        {
        }

        public LogicalConnectorInfo(IDiagramViewModel root, DesignerItemViewModelBase dataItem, SerializableItem serializableItem, string serializableType) : base(root, dataItem, serializableItem, serializableType)
        {
        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new LogicalConnectorInfoItem(this);
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is LogicalConnectorInfoItem designer)
            {
                ConnectorValue = designer.ConnectorValue;
                ConnectorValueType = designer.ConnectorValueType;
                ConnectorString = designer.ConnectorString;
            }
        }

        private double _connectorValue;
        public double ConnectorValue
        {
            get
            {
                return _connectorValue;
            }
            set
            {
                SetProperty(ref _connectorValue, value);
            }
        }

        private string _connectorString;
        public string ConnectorString
        {
            get
            {
                return _connectorString;
            }
            set
            {
                SetProperty(ref _connectorString, value);
            }
        }

        private ConnectorValueType _connectorValueType;
        public ConnectorValueType ConnectorValueType
        {
            get
            {
                return _connectorValueType;
            }
            set
            {
                SetProperty(ref _connectorValueType, value);
            }
        }

        private ConnectorErrorCode _errorCode;
        public ConnectorErrorCode ErrorCode
        {
            get
            {
                return _errorCode;
            }
            set
            {
                SetProperty(ref _errorCode, value);
            }
        }

        private string _errorMessage;
        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                SetProperty(ref _errorMessage, value);
            }
        }

        public bool InitValue
        {
            get; set;
        }

        public bool ConnectorChanged
        {
            get; set;
        }

        protected override void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (IsLoaded == false) { return; }

            switch (e.PropertyName)
            {
                case nameof(ConnectorValue):
                case nameof(ConnectorString):
                case nameof(ConnectorValueType):
                case nameof(ErrorCode):
                case nameof(ErrorMessage):
                    RaisePropertyChanged(nameof(ConnectorChanged));
                    break;
            }

        }

        public override bool CanAttachTo(ConnectorInfoBase port)
        {
            if (!base.CanAttachTo(port))
            {
                return false;
            }

            if (port is LogicalConnectorInfo logical)
            {
                if (logical.ConnectorValueType == ConnectorValueType.String && this.ConnectorValueType == logical.ConnectorValueType)
                {
                    return true;
                }
                else if (logical.ConnectorValueType == ConnectorValueType.JsonString && this.ConnectorValueType == logical.ConnectorValueType)
                {
                    return true;
                }
                else if (logical.ConnectorValueType <= ConnectorValueType.ValueType && this.ConnectorValueType <= ConnectorValueType.ValueType)
                {
                    return true;
                }
            }

            return false;

        }
    }
}
