﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DiagramViewModelHelper
    {
        public static DesignerItemViewModelBase GetConnectorDataItem(IEnumerable<SelectableDesignerItemViewModelBase> items, Guid conectorDataItemId, Type connectorDataItemType)
        {
            DesignerItemViewModelBase dataItem = items.OfType<DesignerItemViewModelBase>().Single(x => x.Id == conectorDataItemId);
            return dataItem;
        }

        public static RectangleBase GetBoundingRectangle(IEnumerable<DesignerItemViewModelBase> items)
        {
            double x1 = Double.MaxValue;
            double y1 = Double.MaxValue;
            double x2 = Double.MinValue;
            double y2 = Double.MinValue;

            foreach (DesignerItemViewModelBase item in items)
            {
                x1 = Math.Min(item.Left, x1);
                y1 = Math.Min(item.Top, y1);

                x2 = Math.Max(item.Left + item.ItemWidth, x2);
                y2 = Math.Max(item.Top + item.ItemHeight, y2);
            }

            return new RectangleBase(new PointBase(x1, y1), new PointBase(x2, y2));
        }

        public static RectangleBase GetBoundingRectangle(DesignerItemViewModelBase item)
        {
            return GetBoundingRectangle(new DesignerItemViewModelBase[] { item });
        }
    }
}
