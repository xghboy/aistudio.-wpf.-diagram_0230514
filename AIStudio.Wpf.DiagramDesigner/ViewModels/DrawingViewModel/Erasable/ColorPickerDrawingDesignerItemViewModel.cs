﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ColorPickerDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public ColorPickerDrawingDesignerItemViewModel()
        {
        }

        public ColorPickerDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, bool erasable) : base(root, DrawMode.ErasableLine, startPoint, erasable)
        {
        }

        public ColorPickerDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public ColorPickerDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            var point = CursorPointManager.GetCursorPosition();
            Root.CurrentColor = ColorPickerManager.GetColor(point.X, point.Y);
            return true;
        }

        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            _service.DrawModeViewModel.DrawingColorViewModel.LineColor.Color = Root.CurrentColor;
            _service.DrawModeViewModel.ResetDrawMode();

            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            return true;
        }
    }
}
