﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class TextDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public TextDrawingDesignerItemViewModel()
        {
        }

        public TextDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, bool erasable) : base(root, DrawMode.ErasableRectangle, startPoint, erasable)
        {
            AddTextBox();
        }

        public TextDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public TextDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            return true;
        }

        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {           
            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {    
            return base.OnMouseUp(sender, e);
        }

        protected override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);

            CustomText = true;
        }

        TextDesignerItemViewModel _previewTextDesign;
        public void AddTextBox()
        {
            _previewTextDesign = new TextAutoDesignerItemViewModel(this.Root);
            _previewTextDesign.FontViewModel = CopyHelper.Mapper(this.FontViewModel);
            _previewTextDesign.FontViewModel.FontColor = this.ColorViewModel.LineColor.Color;
            _previewTextDesign.FontViewModel.HorizontalAlignment = HorizontalAlignment.Left;
            _previewTextDesign.FontViewModel.VerticalAlignment = VerticalAlignment.Top;
            _previewTextDesign.Left = Points[0].X;
            _previewTextDesign.Top = Points[0].Y;
            Root?.Add(_previewTextDesign, true);
            _previewTextDesign.PropertyChanged += _previewTextDesign_PropertyChanged;
        }

        private void _previewTextDesign_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName== nameof(IsEditing) && _previewTextDesign.IsEditing == false)
            {
                _previewTextDesign.PropertyChanged -= _previewTextDesign_PropertyChanged;
                Root?.Remove(_previewTextDesign);
           
                Text = _previewTextDesign?.Text;              
                if (!string.IsNullOrEmpty(Text))
                {              
                    var typeface = new Typeface(new FontFamily(FontViewModel.FontFamily), FontViewModel.FontStyle, FontViewModel.FontWeight, FontViewModel.FontStretch);
                    var formattedText = new FormattedText(_previewTextDesign?.Text,
                        System.Globalization.CultureInfo.InvariantCulture,
                        FlowDirection.LeftToRight,
                    typeface,
                    FontViewModel.FontSize,
                    new SolidColorBrush(FontViewModel.FontColor));

                    Geometry = formattedText.BuildGeometry(new Point()).GetFlattenedPathGeometry();
                    IsFinish = true;

                    this.ItemWidth = _previewTextDesign.ItemWidth;
                    this.ItemHeight = _previewTextDesign.ItemHeight;
                    this.Left = _previewTextDesign.Left + 2 + Geometry.Bounds.Left;
                    this.Top = _previewTextDesign.Top + 2 + Geometry.Bounds.Top;
                    this.Root?.AddItemCommand.Execute(this);
                }               
            }
        }
    }
}
