﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using AIStudio.Wpf.DiagramDesigner.ViewModels;
using AIStudio.Wpf.DiagramDesigner.ViewModels.BaseViewModel;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DrawingRubberbandAdorner : Adorner
    {
        private Brush _rubberbandBrush;
        private Pen _rubberbandPen;

        private DesignerCanvas _designerCanvas;

        private IDiagramViewModel _viewModel
        {
            get
            {
                return _designerCanvas.DataContext as IDiagramViewModel;
            }
        }
        private IDiagramServiceProvider _service
        {
            get
            {
                return DiagramServicesProvider.Instance.Provider;
            }
        }

        private DrawMode DrawMode
        {
            get
            {
                if (_viewModel.DrawModeViewModel != null)
                {
                    return _viewModel.DrawModeViewModel.GetDrawMode();
                }
                else
                {
                    return _service.DrawModeViewModel.GetDrawMode();
                }
            }
        }

        private DrawingDesignerItemViewModelBase _drawingDesignerItem;
        public DrawingRubberbandAdorner(DesignerCanvas designerCanvas, Point dragStartPoint)
            : base(designerCanvas)
        {
            this._designerCanvas = designerCanvas;
            this._designerCanvas.Focus();

            if (DrawMode == DrawMode.Eraser)
            {
                _drawingDesignerItem = new EraserDrawingDesignerItemViewModel(_viewModel, dragStartPoint);

                _rubberbandBrush = null;
                _rubberbandPen = new Pen(new SolidColorBrush(Colors.Red) { Opacity = 0.5 }, _drawingDesignerItem.ColorViewModel.LineWidth);
            }
            else if (DrawMode == DrawMode.EraserPreview)
            {
                _drawingDesignerItem = new EraserPreviewDrawingDesignerItemViewModel(_viewModel, dragStartPoint);

                _rubberbandBrush = null;
                _rubberbandPen = new Pen(new SolidColorBrush(Colors.Red) { Opacity = 0.5 }, _drawingDesignerItem.ColorViewModel.LineWidth);
            }
            else if (DrawMode >= DrawMode.ColorPicker && DrawMode <= DrawMode.ErasableDirectLine)
            {
                if (DrawMode == DrawMode.ErasableLine)
                {
                    _drawingDesignerItem = new LineDrawingDesignerItemViewModel(_viewModel, dragStartPoint, true);
                }
                else if (DrawMode == DrawMode.ErasableRectangle)
                {
                    _drawingDesignerItem = new RectangleDrawingDesignerItemViewModel(_viewModel, dragStartPoint, true);
                }
                else if (DrawMode == DrawMode.ErasableEllipse)
                {
                    _drawingDesignerItem = new EllipseDrawingDesignerItemViewModel(_viewModel, dragStartPoint, true);
                }
                else if (DrawMode == DrawMode.ErasablePolyline)
                {
                    _drawingDesignerItem = new PolylineDrawingDesignerItemViewModel(_viewModel, dragStartPoint, true);
                }
                else if (DrawMode == DrawMode.ErasablePolygon)
                {
                    _drawingDesignerItem = new PolygonDrawingDesignerItemViewModel(_viewModel, dragStartPoint, true);
                }
                else if (DrawMode == DrawMode.ErasableDirectLine)
                {
                    _drawingDesignerItem = new DirectLineDrawingDesignerItemViewModel(_viewModel, dragStartPoint, true);
                }
                else if (DrawMode == DrawMode.ErasableText)
                {
                    _drawingDesignerItem = new TextDrawingDesignerItemViewModel(_viewModel, dragStartPoint, true);
                }
                else if (DrawMode == DrawMode.ColorPicker)
                {                   
                    _drawingDesignerItem = new ColorPickerDrawingDesignerItemViewModel(_viewModel, dragStartPoint, true);
                }

                _rubberbandBrush = null;//ColorObject.ToBrush(_drawingDesignerItem.ColorViewModel.FillColor);
                _rubberbandPen = new Pen(_drawingDesignerItem.ColorViewModel.LineColor.ToBrush(), _drawingDesignerItem.ColorViewModel.LineWidth);
                _rubberbandPen.DashStyle = new DashStyle(StrokeDashArray.Dash[(int)_drawingDesignerItem.ColorViewModel.LineDashStyle], 1);
            }
            else if (DrawMode >= DrawMode.Line && DrawMode <= DrawMode.DirectLine)
            {
                if (DrawMode == DrawMode.Line)
                {
                    _drawingDesignerItem = new LineDrawingDesignerItemViewModel(_viewModel, dragStartPoint, false);
                }
                else if (DrawMode == DrawMode.Rectangle)
                {
                    _drawingDesignerItem = new RectangleDrawingDesignerItemViewModel(_viewModel, dragStartPoint, false);
                }
                else if (DrawMode == DrawMode.Ellipse)
                {
                    _drawingDesignerItem = new EllipseDrawingDesignerItemViewModel(_viewModel, dragStartPoint, false);
                }
                else if (DrawMode == DrawMode.Polyline)
                {
                    _drawingDesignerItem = new PolylineDrawingDesignerItemViewModel(_viewModel, dragStartPoint, false);
                }
                else if (DrawMode == DrawMode.Polygon)
                {
                    _drawingDesignerItem = new PolygonDrawingDesignerItemViewModel(_viewModel, dragStartPoint, false);
                }
                else if (DrawMode == DrawMode.DirectLine)
                {
                    _drawingDesignerItem = new DirectLineDrawingDesignerItemViewModel(_viewModel, dragStartPoint, false);
                }

                _rubberbandBrush = _drawingDesignerItem.ColorViewModel.FillColor.ToBrush();
                _rubberbandPen = new Pen(_drawingDesignerItem.ColorViewModel.LineColor.ToBrush(), _drawingDesignerItem.ColorViewModel.LineWidth);
                _rubberbandPen.DashStyle = new DashStyle(StrokeDashArray.Dash[(int)_drawingDesignerItem.ColorViewModel.LineDashStyle], 1);
            }

            if (_drawingDesignerItem == null)
            {

            }
        }

        protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
        {
            if (_drawingDesignerItem?.OnMouseMove(this, e) == true)
            {
                if (!this.IsMouseCaptured)
                    this.CaptureMouse();

                this.InvalidateVisual();
            }
            else
            {
                if (this.IsMouseCaptured) this.ReleaseMouseCapture();
            }

            e.Handled = true;
        }

        protected override void OnMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_drawingDesignerItem?.OnMouseUp(this, e) == false)
            {
                return;
            }

            // release mouse capture
            if (this.IsMouseCaptured) this.ReleaseMouseCapture();

            // remove this adorner from adorner layer
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this._designerCanvas);
            if (adornerLayer != null)
                adornerLayer.Remove(this);

            if (_drawingDesignerItem?.IsFinish == true)
            {
                _viewModel.AddItemCommand.Execute(_drawingDesignerItem);
            }

            this._service.DrawModeViewModel.ResetDrawMode();
            e.Handled = true;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            _drawingDesignerItem?.OnMouseDown(this, e);

            e.Handled = true;
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            // without a background the OnMouseMove event would not be fired !
            // Alternative: implement a Canvas as a child of this adorner, like
            // the ConnectionAdorner does.
            dc.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));

            dc.DrawGeometry(_rubberbandBrush, _rubberbandPen, _drawingDesignerItem?.Geometry);
        }
    }
}
